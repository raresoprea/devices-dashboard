import React from 'react'
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(() => ({
  header: {
    display: 'flex',
    flexDirection: 'column',
    height: '20px',
    background: 'lightgray',
    fontSize: '16px',
    margin: '10px',
    minWidth: '30%'
  },
}));

export const DeviceHeader = (props) =>  (
  <h2 className={useStyles().header}>
    {props.children}
  </h2>
);
