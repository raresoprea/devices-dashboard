import React from 'react'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
}));

export const Row = (props) =>  {
  return (<div className={useStyles().row}>
    {props.children}
  </div>
);
}