import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Col } from './Col'
import { Row } from './Row'
import { DeviceHeader } from './DeviceHeader'
import { TextareaAutosize, TextField } from '@material-ui/core'


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    padding: '10px 20px',
    fontFamily: 'CircularStd',
    boxShadow: '0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22)',
    borderRadius: '12px',
    margin: '20px 10px',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    background: 'linear-gradient(90deg, rgba(255,255,255,0.17130602240896353) 0%, rgba(188,226,241,1) 100%)',
    color: 'black',
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    color: 'white',
    background: '#3065ff',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  textField: {
    margin: '5px',
    fontSize: '12px'
  },
  description: {
    minWidth: '30%',
    marginTop: '5px',
    background: 'lightgrey'
  }
}));

export const DeviceInformation = ({ name, model, imei, eid, description, serialNumber }) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <DeviceHeader>
        Device information
      </DeviceHeader>
      <Col>
        <Row>
          <Col>
            <TextField label="Name" value={name} variant="filled" className={classes.textField} />
            <TextField label="Model" value={model} variant="filled" className={classes.textField} />
          </Col>
          <Col>
            <Row>
              <TextField label="IMEI" value={imei} variant="filled" className={classes.textField} />
              <TextField label="EID" value={eid} variant="filled" className={classes.textField} />
            </Row>
            <TextField label="Serial Number" value={serialNumber} variant="filled" className={classes.textField} />
          </Col>
          <TextareaAutosize
            aria-label="minimum height"
            minRows={8}
            placeholder={description}
            className={classes.description}
            value={description}
          />
        </Row>
        <Row>
          <Col>
            <TextField label="Name" value={name} variant="filled" className={classes.textField} />
            <TextField label="Model" value={model} variant="filled" className={classes.textField} />
          </Col>
        </Row>
        <Row>
          {/*company related info section*/}
          {/*ownership data section*/}
        </Row>
      </Col>
    </div>
  );
}
