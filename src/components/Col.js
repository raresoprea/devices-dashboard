import React from 'react'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  row: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
}));

export const Col = (props) =>  (
  <div className={useStyles().row}>
    {props.children}
  </div>
);
